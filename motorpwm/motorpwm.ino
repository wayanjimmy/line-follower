void setup() {
  // put your setup code here, to run once:

  pinMode(10, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:

  analogWrite(10, 255);

  digitalWrite(8, HIGH);
  digitalWrite(9, LOW);

  delay(10000);

  // change direction

  digitalWrite(10, LOW);

  delay(200);

  analogWrite(10, 255);

  digitalWrite(8, LOW);
  digitalWrite(9, HIGH);

  delay(10000);
}
